export const reviews = [
  {
    id: 1,
    title: "Отличный курс!",
    description:
      "Очень понравилось изучать материалы этого курса. Все объяснено доступно и понятно. Спасибо!",
    grade: 5.0,
    date: Date.now(),
    userFullname: "Екатерина Иванова",
    userEmail: "ekaterina.ivanova@example.com",
    userImageUrl:
      "https://images.pexels.com/photos/1036623/pexels-photo-1036623.jpeg?auto=compress&cs=tinysrgb&w=800",
  },
  {
    id: 2,
    title: "Очень полезный курс",
    description:
      "С помощью этого курса я смог значительно улучшить свои навыки в веб-разработке. Рекомендую!",
    grade: 4.5,
    date: Date.now() - 86400000, 
    userFullname: "Алексей Петров",
    userEmail: "alexey.petrov@example.com",
    userImageUrl:
      "https://images.pexels.com/photos/1680175/pexels-photo-1680175.jpeg?auto=compress&cs=tinysrgb&w=1260&h=750&dpr=2",
  },
  {
    id: 3,
    title: "Отличный преподаватель!",
    description:
      "Очень благодарен автору курса за ясное и понятное изложение материала. Рекомендую всем!",
    grade: 4.8,
    date: Date.now() - 172800000, 
    userFullname: "Мария Сидорова",
    userEmail: "maria.sidorova@example.com",
    userImageUrl:
      "https://images.pexels.com/photos/774909/pexels-photo-774909.jpeg?auto=compress&cs=tinysrgb&w=1260&h=750&dpr=2",
  },
];

export default reviews;
