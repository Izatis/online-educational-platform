const categories = [
  {
    id: 1,
    title: "Математика",
    imageName: "Mathematics",
    imageUrl:
      "https://images.pexels.com/photos/1329302/pexels-photo-1329302.jpeg",
  },
  {
    id: 2,
    title: "Литература",
    imageName: "Literature",
    imageUrl:
      "https://images.pexels.com/photos/46274/pexels-photo-46274.jpeg?auto=compress&cs=tinysrgb&w=1260&h=750&dpr=2",
  },
  {
    id: 3,
    title: "История",
    imageName: "History",
    imageUrl:
      "https://images.pexels.com/photos/356966/pexels-photo-356966.jpeg?auto=compress&cs=tinysrgb&w=1260&h=750&dpr=2",
  },
  {
    id: 4,
    title: "Биология",
    imageName: "Biology",
    imageUrl:
      "https://images.pexels.com/photos/751682/pexels-photo-751682.jpeg?auto=compress&cs=tinysrgb&w=800",
  },
  {
    id: 5,
    title: "Физика",
    imageName: "Physics",
    imageUrl:
      "https://images.pexels.com/photos/714698/pexels-photo-714698.jpeg?auto=compress&cs=tinysrgb&w=1260&h=750&dpr=2",
  },
  {
    id: 6,
    title: "Химия",
    imageName: "Chemistry",
    imageUrl:
      "https://images.pexels.com/photos/2280571/pexels-photo-2280571.jpeg?auto=compress&cs=tinysrgb&w=1260&h=750&dpr=2",
  },
  {
    id: 7,
    title: "Иностранные языки",
    imageName: "Foreign Languages",
    imageUrl:
      "https://images.pexels.com/photos/18069696/pexels-photo-18069696.png?auto=compress&cs=tinysrgb&w=800",
  },
  {
    id: 8,
    title: "Информатика",
    imageName: "Computer Science",
    imageUrl:
      "https://images.pexels.com/photos/177598/pexels-photo-177598.jpeg?auto=compress&cs=tinysrgb&w=1260&h=750&dpr=2",
  },
  {
    id: 9,
    title: "География",
    imageName: "Geography",
    imageUrl:
      "https://images.pexels.com/photos/2319032/pexels-photo-2319032.jpeg?auto=compress&cs=tinysrgb&w=800",
  },
  {
    id: 10,
    title: "Экономика",
    imageName: "Economics",
    imageUrl:
      "https://images.pexels.com/photos/13102469/pexels-photo-13102469.jpeg?auto=compress&cs=tinysrgb&w=1260&h=750&dpr=2",
  },
  {
    id: 11,
    title: "Философия",
    imageName: "Philosophy",
    imageUrl:
      "https://images.pexels.com/photos/7956373/pexels-photo-7956373.jpeg?auto=compress&cs=tinysrgb&w=800",
  },
  {
    id: 12,
    title: "Искусство",
    imageName: "Art",
    imageUrl:
      "https://images.pexels.com/photos/1209843/pexels-photo-1209843.jpeg?auto=compress&cs=tinysrgb&w=800",
  },
];

export default categories;
