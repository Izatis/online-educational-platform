export interface IReview {
  id: number;
  title: string;
  description: string;
  grade: number;
  date: number;
  userFullname: string;
  userEmail: string;
  userImageUrl: string;
}
