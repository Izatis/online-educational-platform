import React, { FC, useEffect } from "react";
import s from "./MyModalVideo.module.scss";

import { Form, Input, Modal } from "antd";
import { ILesson } from "@/redux/types/lesson";

interface IMyModalVideoProps {
  lesson: ILesson;
  isModalOpen: boolean;
  setIsModalOpen: (active: boolean) => void;
}

const MyModalVideo: FC<IMyModalVideoProps> = ({
  lesson,
  isModalOpen,
  setIsModalOpen,
}) => {
  const [form] = Form.useForm();
  useEffect(() => {
    form.setFieldsValue({ ...form.getFieldsValue() });
  }, []);
  const handleSubmit = async (values: any) => {};
  return (
    <Modal
      title={lesson.title}
      open={isModalOpen}
      onOk={() => setIsModalOpen(false)}
      onCancel={() => setIsModalOpen(false)}
    >
      <video
        className={s.video}
        controls
        poster="https://images.pexels.com/photos/1779487/pexels-photo-1779487.jpeg?auto=compress&cs=tinysrgb&w=1260&h=750&dpr=2"
        height={300}
        loop
      >
        <source src="/video.mp4" type='video/ogg; codecs="theora, vorbis"' />
        <source
          src="/video.mp4"
          type='video/mp4; codecs="avc1.42E01E, mp4a.40.2"'
        />
        <source src="/video.mp4" type='video/webm; codecs="vp8, vorbis"' />
        <source type="video/mp4" src="/video.mp4" />
      </video>

      <b>Оставьте комментарию</b>
      <Form form={form} name="form" onFinish={handleSubmit}>
        <Form.Item
          className={s.authorization__deIndenting}
          name="title"
          rules={[
            {
              required: true,
              message: "Пожалуйста, введите поле",
            },
          ]}
        >
          <Input placeholder="title" />
        </Form.Item>
        <Form.Item
          className={s.authorization__deIndenting}
          name="description"
          rules={[
            {
              required: true,
              message: "Пожалуйста, введите поле",
            },
          ]}
        >
          <Input placeholder="description" />
        </Form.Item>
      </Form>

      <p className={s.video__description}>{lesson.description}</p>
    </Modal>
  );
};

export default MyModalVideo;
