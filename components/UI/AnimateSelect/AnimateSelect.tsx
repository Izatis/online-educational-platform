import { useState, FC } from "react";
import s from "./AnimateSelect.module.scss";

import cn from "classnames";

import MyModalVideo from "@/components/Modals/MyModalVideo/MyModalVideo";
import { Tooltip } from "antd";

interface IAnimateSelectProps {
  section: any;
  isPurchase: boolean | null;
}

const AnimateSelect: FC<IAnimateSelectProps> = ({ section, isPurchase }) => {
  const [selectedLessonId, setSelectedLessonId] = useState<number | null>(null);
  const [lessons, setLessons] = useState([
    {
      id: 1,
      title: "Основы HTML",
      description:
        "Описание: Этот курс предназначен для тех, кто только начинает изучать веб-разработку. Вы узнаете основные элементы языка разметки HTML и научитесь создавать структуру веб-страницы.",
    },
    {
      id: 2,
      title: "CSS для начинающих",
      description:
        "Описание: В этом курсе вы углубитесь в каскадные таблицы стилей (CSS) и научитесь применять стили к веб-страницам. Вы узнаете о различных свойствах CSS, селекторах и способах организации стилей.",
    },
    {
      id: 3,
      title: "JavaScript: введение",
      description:
        "Описание: JavaScript является одним из основных языков программирования для веб-разработки. В этом курсе вы познакомитесь с основами языка, узнаете о переменных, условных операторах, циклах и функциях.",
    },
    {
      id: 4,
      title: "Работа с Git и GitHub",
      description:
        "Описание: Git - это система контроля версий, а GitHub - популярный веб-сервис для хостинга проектов, использующих Git. В этом курсе вы научитесь использовать Git для управления версиями вашего кода и познакомитесь с основными возможностями GitHub.",
    },
    {
      id: 5,
      title: "Адаптивная вёрстка",
      description:
        "Описание: С развитием мобильных устройств стало важным создавать веб-страницы, которые хорошо отображаются на различных устройствах и экранах. В этом курсе вы изучите основы адаптивной вёрстки с помощью CSS и медиа-запросов.",
    },
  ]);
  const [isModalOpen, setIsModalOpen] = useState<boolean>(false);
  const [reveal, setReveal] = useState<boolean>(false);
  const sectionId = section.id;
  const token = JSON.parse(localStorage.getItem("token") as string);
  const handleClick = (id: number) => {
    setSelectedLessonId(id);
    setIsModalOpen(true);
  };

  // ---------------------------------------------------------------------------------------------------------------------------------
  // GET
  // const { data: lessons = [] } = useToGetLessonsQuery({
  //   token,
  //   sectionId,
  // });

  return (
    <div
      className={cn(s.select, { [s.reveal]: reveal })}
      onClick={() => setReveal(!reveal)}
    >
      <p>{section.name}</p>
      <div
        className={s.select__line}
        onClick={(e) => e.stopPropagation()}
      ></div>
      <div className={s.select__hide} onClick={(e) => e.stopPropagation()}>
        <ul className={s.select__list}>
          {lessons.map((lesson: any) => {
            return (
              <>
                {isPurchase ? (
                  <li
                    className={s.isPurchaseActive}
                    onClick={() => handleClick(lesson.id)}
                  >
                    <Tooltip
                      title="Нажмите чтобы посмотреть курс"
                      placement="rightTop"
                    >
                      {lesson.title}
                    </Tooltip>
                  </li>
                ) : (
                  <li className={s.isPurchaseDisabled}>
                    <Tooltip
                      title="Купите чтобы посмотреть курс"
                      placement="rightTop"
                    >
                      {lesson.title}
                    </Tooltip>
                  </li>
                )}

                <MyModalVideo
                  lesson={lesson}
                  isModalOpen={lesson.id === selectedLessonId && isModalOpen}
                  setIsModalOpen={setIsModalOpen}
                />
              </>
            );
          })}
        </ul>
      </div>
    </div>
  );
};

export default AnimateSelect;
